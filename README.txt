ip2Country is a PHP based class, helps loading or redirecting region-based website, to make sure each area is viewing the currect website.

This class uses ip2country API to determine the country of the visitor, by finding the visitors IP and making sure it is not an internal IP. We are also using ipMango API to find the actual IP on all other methods failed.

Currently we have included US, Canada, Europe and Asia Pacefic regions. You can extend the functionality by adding additional regions and country codes in the class. Examples are given in PHP class.

/********Developed By: Anish Karim***********************
*********<thecoderin@gmail.com>*************************
*********Version: 0.1.1*********************************
*********Last Updated: April 7, 2014********************
********************************************************/